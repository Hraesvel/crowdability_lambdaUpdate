#!/usr/bin/env bash

profile="Crowdability"
if [ $# -gt 0 ]
then
  profile=$1
fi


if ! command -v lambdaManager &> /dev/null
then
  echo "installing lambdaManager"
    pip install --user --editable .
else
  w=$(command -v lambdaManager)
  echo "lambdaManager already installed at: $w"
  echo "if running the command doesn't prompt anything, please ensure your PATH are point to where the executable is installed"
fi