# Automation Setup

This contains 2 scripts that help to update layer `endpointPingTools`

- `check_endpoint.py`
    - compare the local file with that of the latest version on AWS and will upload a new version if necessary
- `lambda_function.py`
    - Script will look at each lambda function in file `lambdaFunctions.json` to see if the layer configuration is up to
      date
    - if a function is not using the latest layer version this will update the function configuration.


dependencies:

- python3
- zip
- unzip
- AWS CLI
- autopep8
- boto3

```bash
sudo apt-get install -y zip unzip
pip install autopep8 boto3
```

## Setup

AWS CLI v2 is to configure
credentials, [installation guild](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
additionally setting up credentials require an user with access tokens with permission to edit lambda service. The
script assumes that credential info is saved to profile `Crowdability`.

This is performed with the command below

```bash
AWS configure --profile Crowdability
```

## Usage
`Todo: combine both scripts into 1 with a simple way to pass command-line argument.`

 trigger order:
1. `./endpointLayer.py`
2. `./lambdaFunctions.py` 
