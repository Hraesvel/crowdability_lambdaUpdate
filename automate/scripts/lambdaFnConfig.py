#!/usr/bin/env python3
import json
from uuid import uuid4

from botocore.exceptions import ClientError

from automate.scripts.shared import RESOURCE_PATH


class LambdaConfigProcess:
    __session = None
    __lambda_service = None
    __event_service = None
    __roles = None

    def __init__(self, session):
        self.__session = session
        self.__lambda_service = session.client('lambda')
        self.__event_service = session.client('events')
        self.__roles = dict((name['RoleName'], name['Arn']) for name in session.client('iam').list_roles()['Roles'])
        self.__role_key = 'lambdaRole'

    def updateLambdaFnConfigs(self, FunctionName, layerName='endpointPingTools', roleName=None,
                              ConfigJson=(RESOURCE_PATH / 'layerUpload_response.json'),
                              timeout=3, SkipLayer=False, remove_endpoint=False, **garbage):
        """
        update a lambda function layer version and/or role. defaulting to layer 'endpointPingTools' and role 'lambdaRole'
        :param remove_endpoint:
        :param roleName:
        :param SkipLayer:
        :param timeout:
        :param FunctionName: Name of function to configure
        :param layerName: layer FunctionName to update or add to function; defaults to endpointPingTools
        :param ConfigJson: file path, to json file with details about the layer currently uploaded to AWS
        :return: None
        """
        pub_dict = None
        if ConfigJson:
            try:
                # review the most recent uploaded layer details; file 'layerUpload_response.json' inside resource dir
                with open(ConfigJson) as current_layer_file:
                    pub_dict = json.load(current_layer_file)
            except FileNotFoundError:
                print(
                    "layerUpload_response.json doesn't exist. try using the 'endpoint process' command")
                return
        else:
            print("'ConfigJson' can not be empty or None")
            exit(-1)

        fn_info = self.__lambda_service.get_function(FunctionName=FunctionName, Qualifier="$LATEST")

        params = {}
        if timeout > 0:
            params['Timeout'] = timeout

        if roleName and roleName not in fn_info['Configuration']['Role']:
            r = self.__roles.get(roleName)
            if not r:
                print("Error: The 'Role' specified doesn't exist in the AWS IAM")
                exit(-1)
            else:
                params['Role'] = r
                print(f"'Role' specified was found on AWS, and will set function '{FunctionName}' to this role")

        if SkipLayer:
            print("Perform Configuration updates with 'SkipLayer' set to 'True'")
            self.__lambda_service.update_function_configuration(FunctionName=FunctionName,
                                                                **params,
                                                                )

            return

        # Everything below is Layer management

        # Function had no layers
        added_layer = False
        if 'Layers' not in fn_info['Configuration'] and not remove_endpoint:
            print(f"function '{FunctionName}', has no layers, the latest version of 'endpointPingTools' will be added")
            params['Layers'] = [pub_dict['LayerVersionArn']]
            added_layer = True

        elif remove_endpoint:
            if fn_info['Configuration'].get('Layers'):
                print(f"Attempting to removing '{layerName}' layer from function {FunctionName}")
                params['Layers'] = list(ele['Arn'] for ele in
                                        filter(lambda x: layerName not in x['Arn'], fn_info['Configuration']['Layers']))
            #     i = next((index for (index, d) in enumerate(params['Layers']) if layerName in d['Arn']))
            #     fn_info['Configuration']['Layers'].pop(i)
            #     params['Layers'] = fn_info['Configuration']['Layers']
            else:
                params['Layers'] = []

        if params and (added_layer or remove_endpoint):
            print(f"Updating Role and/or adding layer config to '{FunctionName}'")
            self.__lambda_service.update_function_configuration(FunctionName=FunctionName,
                                                                **params
                                                                )
            return
        elif not params:
            print(f"No changes are necessary for function {FunctionName}")
            return

        # function has layers but missing specified one
        old_layer = next(filter(lambda x: layerName in x['Arn'], fn_info['Configuration']['Layers']))['Arn']
        if not old_layer:
            self.addLayerToFunction(fn_info, FunctionName, pub_dict['LayerVersionArn'], Timeout=timeout,
                                    Role=params["Role"], )
            return

        # The function has the specified layer, so check it and then update if needed.
        ver_idx = old_layer.rindex(':')
        fn_layer_ver = old_layer[ver_idx + 1:]

        if not pub_dict['Version'] > int(fn_layer_ver):
            print(f"Function '{FunctionName}' is up to date on layer configuration.")
            return
        print(f'Info: Updating lambda function "{FunctionName}", "endpointPingTools" layer to the latest version')

        layers: list = fn_info['Configuration']['Layers']

        # find the index of the layer to update
        i = next((index for (index, d) in enumerate(layers) if old_layer[:ver_idx] in d['Arn']))

        # update layer
        layers[i]['Arn'] = pub_dict['LayerVersionArn']

        # extract Arn for list of dictionaries
        updated_list = list(v for layer in layers for k, v in layer.items() if k == 'Arn')
        params['Layers'] = updated_list
        self.__lambda_service.update_function_configuration(FunctionName=FunctionName,
                                                            **params
                                                            )
        print('Success')

    def addLayerToFunction(self, data, FunctionName, LayerVersionArn, Timeout=15, **params):
        print('Required layer is missing, latest version of endpointPingTools will be added')
        layers = data['Configuration']['Layers']
        layers.append(LayerVersionArn)
        self.__lambda_service.update_function_configuration(FunctionName=FunctionName,
                                                            Layers=layers,
                                                            Timeout=Timeout,
                                                            **params
                                                            )

    def configureEventRule(self, FunctionName, EventName, ScheduleExpression=None, Enable=True, Create=False, **kwargs):
        """
        configure a functions Event trigger
        :param FunctionName: function to configure
        :param EventName: event to modify or add
        :param ScheduleExpression: Expression
        :param Enable: If Set to 'True' the function will be set to an Enabled state else the invert.
        :param Create: If Rules doesn't exist on AWS and 'ScheduleExpression' is provided
        then the function will create a new AWS CloudWatch Rule with the provided data when set to 'True'
        :param kwargs: Garage collection of redundant Key value pairs.
        :return:
        """
        LambdaConfigProcess.__configEventRuleValidation(FunctionName,
                                                        EventName,
                                                        ScheduleExpression,
                                                        Create)
        rule = None
        function_arn = FunctionName
        if not len(FunctionName.split(':')) >= 6:
            function_arn = self.__lambda_service.get_function(FunctionName=function_arn)['Configuration']['FunctionArn']
        default_sid = f"AWSEvents_{EventName}_Id{str(uuid4())[:8]}"

        # Enable or Disable a Event Trigger
        state = 'lambda:InvokeFunction'
        if not Enable:
            state = 'lambda:DisableInvokeFunction'
        try:
            # ClientError is raise if there are no policies (Event Triggers) attached to the function.
            statement = self.getFnStatement(function_arn, EventName)
            if statement:
                if statement['Action'] == state:
                    print(f'{FunctionName} Trigger state already set to {"Enable" if Enable else "Disabled"}')
                    return
                self.disableEventTrigger(function_arn, EventName, state, statement)
            else:
                try:
                    rule = next(filter(lambda r: EventName in r['Name'], self.__event_service.list_rules()['Rules']))
                except StopIteration:
                    if Create and ScheduleExpression:
                        rule = self.createEventRule(FunctionName, EventName, ScheduleExpression, Enable)
                    else:
                        print(f"Creation Error: {FunctionName}")
                        print("\t'Create' must set to true and 'ScheduleExpression' is not empty")
                        return

                self.setEventTrigger(function_arn, EventName, state, default_sid, rule['RuleArn'])

        # ClientError is raise if there are no policies (Event Triggers) attached to the function.
        except ClientError as e:
            if e.response['Error']['Code'] != "ResourceNotFoundException":
                print(e)
                exit(-1)

            rule = self.getEventRule(EventName, FunctionName, ScheduleExpression, Create=Create)
            self.setEventTrigger(function_arn, EventName, state, default_sid, rule.get('RuleArn') or rule['Arn'])

        print(f"Successful configured {FunctionName} event trigger {EventName}.\n\tTrigger State: {state}")

    @staticmethod
    def __configEventRuleValidation(FunctionName, EventName, ScheduleExpression, Create):
        if not FunctionName:
            print("'FunctionName' must not be blank or None!")
            exit(-1)
        elif not EventName:
            print("'EventName' must not be blank or None!")
            exit(-1)
        elif not ScheduleExpression and Create:
            print("'ScheduleExpression' must not be blank or None if `Create` is set to True")
            exit(-1)

    def getFnStatement(self, function_arn, EventName):
        res = self.__lambda_service.get_policy(FunctionName=function_arn)
        # convert json string value for 'Policy' to dict
        res['Policy'] = json.loads(res['Policy'])
        statement = None
        try:
            statement = next(
                filter(lambda i: f'rule/{EventName}' in i['Condition']['ArnLike']['AWS:SourceArn'],
                       res['Policy']['Statement'])
            )
        except StopIteration:
            pass
        return statement

    def disableEventTrigger(self, function_arn, EventName, state, statement):
        self.__lambda_service.remove_permission(FunctionName=function_arn, StatementId=statement['Sid'], )
        self.setEventTrigger(function_arn, EventName, state, statement.get('Sid'),
                             statement['Condition']['ArnLike']['AWS:SourceArn'])

    def getEventRule(self, EventName, FunctionName=None, ScheduleExpression=None, Create=False, Enabled=True):
        rule = None
        try:
            rule = next(filter(lambda r: EventName in r['Name'], self.__event_service.list_rules()['Rules']))
        except StopIteration:
            if not Create:
                print(
                    "Error: Rule doesn't exist, please check spelling or be sure to add the rule to your AWS CloudWatch")
                exit(-1)
            elif Create:
                if not FunctionName or not ScheduleExpression:
                    print("Error: Function Name and a ScheduleExpression can't be blank if Create is set to true.")
                    exit(-1)
                rule = self.createEventRule(FunctionName, EventName, ScheduleExpression, Enabled)
        return rule

    def createEventRule(self, FunctionName, EventName, ScheduleExpression, Enabled):
        des = f'Schedule rule created for the Lambda function "{FunctionName}".'
        # returns Key value {'RuleARN': string}
        name = EventName.strip()

        rule = self.__event_service.put_rule(Name=name, ScheduleExpression=ScheduleExpression,
                                             State='ENABLED' if Enabled else 'DISABLED',
                                             Description=des
                                             )
        return rule

    def setEventTrigger(self, function_arn, EventName, state, statement_id, source_arn):
        self.__lambda_service.add_permission(FunctionName=function_arn,
                                             StatementId=statement_id,
                                             Action=state,
                                             Principal='events.amazonaws.com',
                                             SourceArn=source_arn)
        self.__event_service.put_targets(
            Rule=EventName,
            Targets=[
                {
                    'Id': "1",
                    'Arn': function_arn
                }
            ]
        )

# if __name__ == "__main__":
#     # check file created by `update_endpointLayer.py` for layer details
#     with open(RESOURCE_PATH / 'layerUpload_response.json') as current_layer_file:
#         local_ver = json.load(current_layer_file)
#
#     with open(RESOURCE_PATH / 'lambdaFunctions.json', 'r') as file:
#         functions = json.load(file)
#         for functionName, value in functions['functionNames'].items():
#             # with open('tmp.json', 'a+') as file:
#             # data = lambda_service.get_function(FunctionName=FunctionName, Qualifier="$LATEST")
#             # file.write(json.dumps(data))
#             updateLambdaFnConfigs(functionName, ConfigJson=local_ver)
#             configureEventRule(functionName, **value, Enable=False, Create=False)
