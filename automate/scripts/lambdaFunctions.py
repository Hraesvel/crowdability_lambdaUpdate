#!/usr/bin/env python3

import filecmp
import json
import os
import shutil
import subprocess
from urllib.request import urlopen

import boto3

from automate.scripts.shared import LAMBDA_FUNCTIONS_LIST, LAMBDA_FUNCTIONS_SRC, TMP_PATH


class LambdaUpdateProcess:
    __lambda_serv = None
    __role_serv = None
    __roles = None
    __role_key = None
    AWS_FN = None

    def __init__(self, session, Role='lambdaRole'):
        self.__lambda_serv = session.client('lambda')
        self.__role_serv = session.client('iam')
        self.__roles = dict((name['RoleName'], name['Arn']) for name in self.__role_serv.list_roles()['Roles'])
        self.__role_key = 'lambdaRole'
        self.AWS_FN = self.fetchFunctionList()

    def fetchFunctionList(self):
        return dict([fn['FunctionName'], fn] for fn in self.__lambda_serv.list_functions()['Functions'])

    def fetchFunctions(self, functionName):
        """
        Downloads the code files of a Lambda function from AWS and unpacks the contents into a temp directory
        :param functionName: function to download
        :return:
        """
        tmp_filename = TMP_PATH / functionName
        os.makedirs(TMP_PATH, exist_ok=True)
        try:
            function_details = self.__lambda_serv.get_function(FunctionName=functionName, Qualifier='$LATEST')
            with open(f'{tmp_filename}.zip', 'wb+') as file:
                with urlopen(function_details['Code']['Location']) as resp:
                    file.write(resp.read())
            shutil.unpack_archive(f'{tmp_filename}.zip', tmp_filename)
            subprocess.run(f'autopep8 -i {tmp_filename}/lambda_function.py', shell=True)

        except Exception as e:
            print(e)

    @staticmethod
    def comparedFiles(left, right, pep8=True):
        """
        Compare local file to that of one on downloaded from AWS.
        :param left: file A
        :param right: file B
        :param pep8:
        :return:
        """
        try:
            if not os.path.exists(left) or not os.path.exists(right):
                raise FileNotFoundError

            if not right:
                raise FileNotFoundError(f"{left} is missing file to compare with, 'right' can't be 'None'")

            if pep8:
                subprocess.run(f'autopep8 -i {left} {right}', shell=True)
            print(f'comparing {os.path.split(left)[-1]}')
            # todo: use file fingerprint or hash such as sha256 to campare
            with open(left, 'rt') as s, open(right, 'rt') as t:
                # if not filecmp.cmp(left, right, shallow=False) or not os.path.exists(right):
                cmp = all(line1 == line2 for line1, line2 in zip(s, t))
                if not cmp or not os.path.exists(right):
                    print('Warning: file do not match or missing!')
                    return False
                else:
                    print('Pass: files match.')
                    return True

        except FileNotFoundError as e:
            print(e)

    def uploadFunctions(self, JsonFile=LAMBDA_FUNCTIONS_LIST):
        """
        Upload lambdaFunctions to cloud if they are missing
        :param JsonFile: A Json file that contains lambda function details
        :return:
        """
        with open(JsonFile) as list_of_fn:
            os.makedirs(TMP_PATH, exist_ok=True)
            fns = json.load(list_of_fn)['functionNames']
            for fn in fns.keys():
                # If a Function Name is not found on AWS Lambda add it
                if fn not in self.AWS_FN.keys():
                    print(f'creating function {fn}')
                    if not os.path.exists(f'{LAMBDA_FUNCTIONS_SRC}/{fn}.py'):
                        print(f"Skip: No such file or directory: '{LAMBDA_FUNCTIONS_SRC}/{fn}.py'")
                        continue
                    shutil.copy(f'{LAMBDA_FUNCTIONS_SRC}/{fn}.py', './lambda_function.py', )
                    # Todo convert to shutil.make_archive()
                    subprocess.run('zip ./lambda.zip ./lambda_function.py', shell=True)
                    shutil.move('./lambda.zip', TMP_PATH / 'lambda.zip')
                    shutil.move('./lambda_function.py', TMP_PATH / 'lambda_function.py')
                    with open(TMP_PATH / 'lambda.zip', 'rb') as files:
                        self.__lambda_serv.create_function(FunctionName=fn, Runtime='python3.7',
                                                           Handler=f'{fn}.lambda_handler',
                                                           Role=self.__roles[self.__role_key],
                                                           Code={'ZipFile': files.read()}
                                                           )
                else:
                    print(f'Skip: Creation process for {fn}, Already on AWS')

            # shutil.rmtree(TMP_PATH, ignore_errors=True)

    def updateFunction(self, functionName):
        """
        update a lambda function with local version
        :param functionName: FunctionName of function to update
        :return: None
        """
        if not os.path.exists(f'{LAMBDA_FUNCTIONS_SRC}/{functionName}.py'):
            print(f"Skip: No such file or directory: '{LAMBDA_FUNCTIONS_SRC}/{functionName}.py'")
        shutil.copy(f'{LAMBDA_FUNCTIONS_SRC}/{functionName}.py', './lambda_function.py', )
        subprocess.run(f'zip ./lambda.zip ./lambda_function.py', shell=True)
        shutil.move('./lambda.zip', TMP_PATH / 'lambda.zip')
        shutil.move('./lambda_function.py', TMP_PATH / 'lambda_function.py')
        with open(TMP_PATH / 'lambda.zip', 'rb') as files:
            self.__lambda_serv.update_function_code(FunctionName=functionName,
                                                    ZipFile=files.read(),
                                                    Publish=True
                                                    )


if __name__ == '__main__':
    import endpointLayer

    l = LambdaUpdateProcess(boto3.session.Session(profile_name='Crowdability'))
    # l.uploadFunctions()
    with open(LAMBDA_FUNCTIONS_LIST) as file:
        functionNames = json.load(file)['functionNames']
        for fn in functionNames:
            l.fetchFunctions(fn)

        for k in functionNames:
            if not l.comparedFiles(LAMBDA_FUNCTIONS_SRC / f'{k}.py', TMP_PATH / f'{k}/lambda_function.py'):
                print(f'diff {k}')
                l.updateFunction(k)

    endpointLayer.LayerProcess.cleanup()
