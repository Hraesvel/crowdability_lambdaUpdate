#!/usr/bin/env python3

"""
This Script will Compare the local files to that of the current active layers
on the AWS Lambda.
At the moment this script assume that the local files are always ahead and any minor changes will
version up the layer on the AWS.
"""

import json
import shutil
import subprocess
from urllib import request

from automate.scripts.shared import ENDPOINT_SRC, LOG_DETAILS_SRC, TMP_PATH, PUB_FILE_PATH


class LayerProcess:
    __session = None
    __lambda_service = None

    def __init__(self, session):
        self.__session = session
        self.__lambda_service = session.client('lambda')

    def fetchEndpointLayer(self, layerName="endpointPingTools"):
        """
        downloads layer and unzip the content into a temp directory
        :param layerName: FunctionName of layer to download
        :return:
        """

        import os
        layers = self.__lambda_service.list_layers()
        endpoint_info = next(filter(lambda x: x['LayerName'] == layerName, layers['Layers']))
        layer = self.__lambda_service.get_layer_version(LayerName=layerName,
                                                        VersionNumber=endpoint_info['LatestMatchingVersion']['Version']
                                                        )
        with open(PUB_FILE_PATH, 'w+') as file:
            file.write(json.dumps(layer))
        # download layer
        with request.urlopen(layer['Content']['Location']) as resp:
            with open("endpoint.zip", 'wb+') as file:
                file.write(resp.read())
            # unzip
            os.makedirs(TMP_PATH / 'endpoint', exist_ok=True)
            shutil.unpack_archive('endpoint.zip', TMP_PATH / 'endpoint')
            shutil.move("endpoint.zip", TMP_PATH / "AWS_endpoint.zip")

    def publishEndpointLayer(self, layerName='endpointPingTools', description="",
                             CompatibleRuntimes=['python3.6', 'python3.7', 'python3.8']):
        with open(TMP_PATH / 'endpoint.zip', 'rb') as file:
            response = self.__lambda_service.publish_layer_version(LayerName=layerName, Description=description,
                                                                   Content={'ZipFile': file.read()},
                                                                   CompatibleRuntimes=CompatibleRuntimes)
            with open(PUB_FILE_PATH, 'w+') as f:
                f.write(json.dumps(response))


    @staticmethod
    def zipEndpoint(endpoint_src="", logdetails_src=""):
        pass
        import os
        os.makedirs(TMP_PATH / 'out/python/', exist_ok=True)
        if not endpoint_src:
            with ENDPOINT_SRC as ep:
                endpoint_src = ep
            with LOG_DETAILS_SRC as ld:
                logdetails_src = ld
        shutil.copy(endpoint_src, TMP_PATH / 'out/python/endpoint.py')
        shutil.copy(logdetails_src, TMP_PATH / 'out/python/log_details.py')
        shutil.make_archive(TMP_PATH / 'endpoint', 'zip', TMP_PATH / 'out')

    @staticmethod
    def cleanup(temp_dir=TMP_PATH):
        shutil.rmtree(temp_dir, ignore_errors=True)

    if __name__ == "__main__":
        import os

        if not shutil.which('autopep8'):
            """
             autopep8 is use to format local and downloaded files so that simple line spacing isn't registered as
             reason to update the layer on AWS.
            """
            print("'autopep8' doesn't exist on this machine, please install using 'pip install autopep8'")
            exit(0)

        elif not shutil.which('zip') or not shutil.which('unzip'):
            print('tools to zip and unzip files are not installed on this machine.')
            print('please use your package manager to install them')
            exit(0)

        fetchEndpointLayer()
        cf = comparedFiles
        with ENDPOINT_SRC as ep:
            with LOG_DETAILS_SRC as ld:
                if not cf(ep, TMP_PATH / 'endpoint/python/endpoint.py') or not \
                        cf(ld, TMP_PATH / 'endpoint/python/log_details.py'):
                    zipEndpoint()
                    print('updating layer')
                    publishEndpointLayer(layerName='endpointPingTools', description="version update")
        cleanup(temp_dir=TMP_PATH)
