from importlib_resources import path, files

import SRC_endpointPingTools
import SRC_lambdaFunction
import automate
from automate import resource

RESOURCE_PATH = files(resource)
ENDPOINT_SRC = path(SRC_endpointPingTools, 'endpoint.py')
LOG_DETAILS_SRC = path(SRC_endpointPingTools, 'log_details.py')
LAMBDA_FUNCTIONS_SRC = files(SRC_lambdaFunction)
LAMBDA_FUNCTIONS_LIST = RESOURCE_PATH.joinpath('lambdaFunctions.json')
PUB_FILE_PATH = RESOURCE_PATH.joinpath('layerUpload_response.json')
TMP_PATH = files(automate) / 'temp'
