import json

import boto3
import click
import colorama
from botocore.exceptions import ProfileNotFound, ClientError, NoCredentialsError

import automate.scripts.endpointLayer as ePt
import automate.scripts.lambdaFnConfig as lCfg
import automate.scripts.lambdaFunctions as lFn
import automate.scripts.shared as shr

EPT: ePt.LayerProcess
CFG: lCfg.LambdaConfigProcess
LFN: lFn.LambdaUpdateProcess


@click.group()
@click.option('--profile-name', '-p')
@click.option('--skip-profile-echo', '-s', is_flag=True)
def cli(profile_name, skip_profile_echo):
    global EPT
    global CFG
    global LFN
    colorama.init()
    try:
        if profile_name is not None and profile_name.strip() == "":
            profile_name = None

        # configure classes
        s = boto3.session.Session(profile_name=profile_name)
        EPT = ePt.LayerProcess(s)
        CFG = lCfg.LambdaConfigProcess(s)
        LFN = lFn.LambdaUpdateProcess(s)

        # quick test to see if profile tokens are valid.
        s.client('iam').get_user()

        if not skip_profile_echo:
            if profile_name:
                click.echo(f"Using Aws Shell Profile '{profile_name}'\n")
            else:
                click.echo(f"Using 'Default' Aws Shell Profile\n")
    except ProfileNotFound as err:
        click.echo(
            "Profile give doesn't exist on your aws shell Credentials\nTo add Credentials run cmd below")
        click.echo(f"To add Credentials run the following `aws configure --profile {profile_name}`")
        click.echo('for more info visit https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html')
        exit(-1)
    except ClientError as err:
        if err.response['Error']['Code'] == 'InvalidClientTokenId':
            click.echo(
                f"An error occurred (InvalidClientTokenId), please be sure that profile '{profile_name}' credentials are correct.")
            exit(-1)
        else:
            click.echo(err)
            exit(-1)
        pass
    except NoCredentialsError or NoCredentialsError as err:
        click.echo("An error occurred using the default aws cli profile. please ensure it's configured correctly")
        click.echo('for more information visit:\n'
                   '\thttps://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html\n'
                   '\thttps://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html#cli-configure-files-where'
                   )
        exit(-1)


@cli.group()
def endpoint():
    pass


@cli.group(name='lambda')
def lambdaFn():
    pass


@cli.command()
def cleanup():
    click.echo("Performing Cleanup process")
    EPT.cleanup()


@click.option('--source_path', default="", help="")
@click.option('--tmp_path', default="", help="")
@click.option('--clean/--no-clean', default=True)
@click.option('--publish/--no-publish', default=True)
@click.option('--layer', '-l', default="endpointPingTools")
@click.option('--description', '-d', default="version update", help="Option to add a description a version")
@endpoint.command(name="process", help="")
def processEndpoint(source_path, tmp_path, clean, publish, layer,description):
    click.echo("Fetch code from layer endpointPingTools")
    EPT.fetchEndpointLayer()
    cf = LFN.comparedFiles
    if not cf(ePt.ENDPOINT_SRC, ePt.TMP_PATH / 'endpoint/python/endpoint.py', ) or not \
            cf(ePt.LOG_DETAILS_SRC, ePt.TMP_PATH / 'endpoint/python/log_details.py'):
        EPT.zipEndpoint(ePt.ENDPOINT_SRC, ePt.LOG_DETAILS_SRC)
        print('updating layer')
        if publish:
            EPT.publishEndpointLayer(layerName=layer, description=description)
    if clean:
        EPT.cleanup()


@lambdaFn.command(name="batch", help="command for comparing local version of files to that of published files")
@click.option('--push', '-p', is_flag=True, help="flag to push local files to AWS if they differ")
@click.option('--add-missing', '-a', is_flag=True, help="flag to upload missing functions")
@click.option('--skip-download', '-s', is_flag=True,
              help='flag to skip downloading code files from AWS for comparing')
@click.option('--force', '-f', is_flag=True, help="flag to force upload of local files to AWS")
@click.option('--timeout', '-t', default=15, type=int,
              help="Option to change the default timeout of a function in seconds")
@click.option('--enable/--disable', ' /-d', default=True, help='Enable/Disable the lambda functions event Triggers')
@click.option('--create/--no-create', '-c/ ', default=False,
              help='Create event trigger rules if they are missing from AWS CloudWatch')
@click.option('--role', '-r', default=None, help="for updating the role for a set of functions")
@click.option('--layer', '-l', default="endpointPingTools")
@click.option('--remove-layer', is_flag=True)
def processLambdaFunctions(push, add_missing, skip_download, force, timeout, enable, create, role, layer, remove_layer):
    if add_missing:
        LFN.uploadFunctions()
    with open(shr.LAMBDA_FUNCTIONS_LIST) as file:
        function_names = json.load(file)['functionNames']
        if not skip_download:
            for fn in function_names:
                LFN.fetchFunctions(fn)

        for fn_name, value in function_names.items():
            if force or not LFN.comparedFiles(shr.LAMBDA_FUNCTIONS_SRC.joinpath(f'{fn_name}.py'),
                                              f'temp/{fn_name}/lambda_function.py'):
                if force or push:
                    LFN.updateFunction(fn_name)
            CFG.updateLambdaFnConfigs(fn_name, ConfigJson=lCfg.RESOURCE_PATH / 'layerUpload_response.json',
                                      timeout=timeout, roleName=role, layerName=layer, remove_endpoint=remove_layer)
            CFG.configureEventRule(fn_name, **value, Enable=enable, Create=create)


@lambdaFn.command(name="single", help="process a single function; use list cmd to view available.")
@click.argument('function_name')
@click.option('--push', '-p', is_flag=True, help="flag to push local files to AWS if they differ")
@click.option('--add-missing', '-a', is_flag=True, help="flag to upload missing functions")
@click.option('--skip-download', '-s', is_flag=True,
              help='flag to skip downloading code files from AWS for comparing')
@click.option('--force', '-f', is_flag=True, help="flag to force upload of local files to AWS")
@click.option('--timeout', '-t', default=15, type=int,
              help="Option to change the default timeout of a function in seconds")
@click.option('--enable/--disable', '/-d', default=True,
              help='Enable/Disable the lambda functions event Triggers, --cfg flag has to be passed')
@click.option('--create/--no-create', '-c/ ', default=False,
              help='Create event trigger rules if they are missing from AWS CloudWatch')
@click.option('--cfg', is_flag=True,
              help='Perform basic configurations on lambda functions')
def processLambdaFunctions(function_name, push, add_missing, skip_download, force, timeout, enable, create, cfg):
    if add_missing:
        LFN.uploadFunctions()
    with open(shr.LAMBDA_FUNCTIONS_LIST) as file:
        fn_name = json.load(file)['functionNames'].get(function_name)
        if not fn_name:
            click.echo(f"'{function_name}' doesn't is not one of the function in the list")
            return
        if not skip_download:
            LFN.fetchFunctions(function_name)
        if force or not LFN.comparedFiles(shr.LAMBDA_FUNCTIONS_SRC.joinpath(f'{function_name}.py'),
                                          f'temp/{function_name}/lambda_function.py'):
            if force or push:
                LFN.updateFunction(function_name)

            if cfg:
                CFG.updateLambdaFnConfigs(function_name, ConfigJson=lCfg.RESOURCE_PATH / 'layerUpload_response.json',
                                          timeout=timeout)
                if "EventName" in fn_name.value():
                    CFG.configureEventRule(function_name, **fn_name.value(), Enable=enable, Create=create)


@lambdaFn.command(name='list', help="List available functions to process")
@click.option('--show-revert', '-r', is_flag=True)
@click.option('--hide-default', '-h', is_flag=True)
def listLambdaFunctions(show_revert, hide_default):
    ls_fn = "Available functions:\n\n"
    fn_names = {}
    if not hide_default:
        with open(shr.LAMBDA_FUNCTIONS_LIST) as file:
            fn_names = json.load(file)['functionNames']
    if show_revert:
        with open(lCfg.RESOURCE_PATH / 'revert_lambdaFunctions.json') as file:
            fn_names.update(json.load(file)['functionNames'])

    for fn in fn_names:
        ls_fn += f'{fn}\n'
    click.echo(ls_fn)


@lambdaFn.command(name='config', help='update')
@click.option('--file', '-f', default=shr.LAMBDA_FUNCTIONS_LIST)
@click.option('-name', '-n', multiple=True, type=str,
              help='Option to process one function from the list of available')
@click.option('--timeout', '-t', default=15, type=int,
              help="Option to change the default timeout of a function in seconds, if 0 or less no "
                   "changes to the function Timeout will be made")
@click.option('--enable/--disable', '-e/-d', default=True, help='Enable/Disable the lambda functions event Triggers')
@click.option('--create', '-c/ ', is_flag=True,
              help='Create event trigger rules if they are missing from AWS CloudWatch')
@click.option('--role', '-r', default=None, help="Option for updating the role for a set of functions")
@click.option('--remove-endpoint', is_flag=True, help="Flag to remove the 'endpointPingTools' layer from a set "
                                                      "of functions")
@click.option('-skip-layer', '-s', is_flag=True, help="Flag to skip the layer management process")
def updateLambdaFunctionsConfigs(enable, name, timeout, create, file, role, remove_endpoint, skip_layer):
    with open(file) as f:
        function_names: dict = json.load(f)['functionNames']
    if name and name not in function_names.keys():
        click.echo(f"A function name provided doesn't exist in the available list.")
        exit(-1)

    for fn_name, value in function_names.items():
        CFG.updateLambdaFnConfigs(fn_name, ConfigJson=lCfg.RESOURCE_PATH / 'layerUpload_response.json',
                                  timeout=timeout, roleName=role,
                                  remove_endpoint=remove_endpoint, SkipLayer=skip_layer,
                                  **value
                                  )
        CFG.configureEventRule(fn_name, **value, Enable=enable, Create=create)


@lambdaFn.command(name='revert', help='Command to revert back to the old lambda functions')
@click.option('-name', '-n', multiple=True, type=str,
              help="Option to process on specific functions from the list of available")
def revertOldLambdaFunctions(name):
    with open(lCfg.RESOURCE_PATH / 'revert_lambdaFunctions.json') as file:
        function_names: dict = json.load(file)['functionNames']
    if name:
        check = all(ele in function_names.keys() for ele in name)
        if not check:
            click.echo(f"`A function name provided doesn't exist in the available list.")
            exit(-1)

        function_names = dict(filter(lambda fn: fn.keys() in name for fn in function_names))

    for fn_name, value in function_names.items():
        CFG.configureEventRule(fn_name, **value, Enable=True)

    with open(shr.LAMBDA_FUNCTIONS_LIST) as file:
        function_names: dict = json.load(file)['functionNames']
        for fn_name, value in function_names.items():
            CFG.configureEventRule(fn_name, **value, Enable=False)


@lambdaFn.command(name='disable', help='Command to disable specific lambda functions')
@click.argument('name', nargs=-1, type=str)
@click.option("-a", '--all', 'all_', is_flag=True,
              help='Flag to disable all functions in the available list including the ones under revert.')
def disableLambdaFunctions(name, all_):
    with open(shr.LAMBDA_FUNCTIONS_LIST) as f, open(shr.RESOURCE_PATH / 'revert_lambdaFunctions.json') as r:
        function_names: dict = json.load(f)['functionNames']
        function_names.update(json.load(r)['functionNames'])

    filter_success = False
    if name and all_:
        click.echo(
            click.style("Warning: ",
                        fg='yellow') + "name/s were give and the Flag --all (-a, all_) was passed, please remove name/s or --all flag")
        exit(0)
    elif not name and not all_:
        click.echo(
            click.style("Warning: ", fg='yellow') + "No name/s were give and the Flag --all (-a, all_) was not passed")
        exit(0)
    elif name:
        filter_success = check = all(ele in function_names.keys() for ele in name)
        if not check:
            click.echo(
                click.style("Error: ", fg='red') + "A function name provided doesn't exist in the available list.")
            exit(-1)

        function_names = dict(filter(lambda fn: fn[0] in name, function_names.items()))
        filter_success = True

    if filter_success or all_:
        for fn_name, value in function_names.items():
            CFG.configureEventRule(fn_name, **value, Enable=False)
