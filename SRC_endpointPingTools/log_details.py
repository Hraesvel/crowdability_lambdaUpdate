#!/usr/bin/env python3
"""
Simple logger
"""


class LogDetails:
    _instance = None
    __info = None
    __warn = None
    __err = None
    __out: str = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(LogDetails, cls).__new__(cls)
        return cls

    @classmethod
    def instance(cls):
        if not cls._instance:
            cls._instance = cls.__new__(cls)
            cls._instance.append_info("Creating Logger...")
        return cls._instance

    def __str__(self) -> str:
        self.__out = "Log details\n"
        self.__out += self.str_count() + '\n'

        if self.__info:
            self.__out += "Info:\n"
            self.get_detail('info')
            self.__out += '\n'

        if self.__warn:
            self.__out += "Warning:\n"
            self.get_detail('warning')
            self.__out += '\n'

        if self.__err:
            self.__out += "Error:\n"
            self.get_detail('error')
        return self.__out

    def __repr__(self):
        return self

    def str_count(self):
        return f"Count - Info: {len(self.__info) if self.__info else 0}, " \
               f"Warning: {len(self.__warn) if self.__warn else 0}, " \
               f"Error: {len(self.__err) if self.__err else 0}"

    def get_detail(self, name: str):
        for info in getattr(self, name):
            self.__out += '\t' + '- ' + info + '\n'

    def clear(self):
        self.__info = None
        self.__warn = None
        self.__err = None
        self.__out = None

    def append_info(self, var: str):
        if self.__info is None:
            self.__info = []
        self.__info.append(var)

    def append_warning(self, var: str):
        if self.__warn is None:
            self.__warn = []
        self.__warn.append(var)

    def append_error(self, var: str):
        if self.__err is None:
            self.__err = []
        self.__err.append(var)

    @property
    def info(self):
        return self.__info

    @property
    def warning(self):
        return self.__warn

    @property
    def error(self):
        return self.__err


# define shared global variable
log = LogDetails().instance()
