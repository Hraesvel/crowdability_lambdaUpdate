#!/usr/bin/env python3
import re
from urllib import request, parse, error
from urllib.request import Request

from log_details import log

"""
django url validation regex (https://github.com/django/django/blob/stable/1.3.x/django/core/validators.py#L45):
"""
regex = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    # domain...
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


class TryPing:
    """
    Interfacing class for try_ping method
    """

    def try_ping(self):
        pass


class EndPoint(TryPing):
    """
    Simple class that construct a uri to be ping with params if provided
    """

    __uri = None
    __params = None
    __name = None
    __headers = None

    def __init__(self, uri: str, name: str = None, headers: dict = None, **params):
        """
        Initialises object, used by the __new__ method to assign variables to designated attributes.
        :param uri: endpoint to ping
        :param name: Identifier for logging
        :param headers: dictionary to pass as header
        :param params: parameters to tack on the end of the URI
        """

        self.__name = name
        self.__uri = uri
        self.__headers = headers
        if 'name' in params.keys():
            params.pop('name')
        if params:
            self.__params = params

    @staticmethod
    def __new__(cls, uri, name: str = None, headers: dict = None, **params):
        """
        Method does an exhaustive validation for creating an EndPoint Object,
        This method will opt to log any Errors or Warnings as oppose to raising an exception, to prevent interruptions.
        :param uri: URI to pinged
        :param name: Identifying name to be used for logging
        :param headers: headers to be sent to a endpoint such as Auth or Api keys
        :param params: key, value pair for params, to be included when pinging the endpoint — such as an api key
        :return: EndPoint else _InvalidEndpoint
        """

        # assign as name as an identifier
        ident = name or list(
            filter(lambda x: x, parse.urlsplit(uri).path[1:].split('/')))[-1]

        if not uri:
            if ident:
                log.append_error(f"uri argument for '{ident}' pass is empty")
            else:
                log.append_error("uri argument pass is empty")
            return _InvalidEndpoint()
        elif not isinstance(uri, str):
            log.append_error(f"uri for {ident}, is not a str")
            return _InvalidEndpoint()

        # Check for malformed URI
        if not re.match(regex, uri):
            log.append_error(f"URI malformed: ['{ident}' : {uri}]")
            return _InvalidEndpoint()

        if not name:
            log.append_warning(
                f"'name' value wasn't provided, 'name' will be set from uri path: '{ident}'")

        instance = object.__new__(cls)
        instance.__init__(uri, name=ident, headers=headers, **params)
        log.append_info(f"EndPoint created for '{ident}'")
        return instance

    def try_ping(self):
        """
        Method that pings the endpoint assign to the object
        :return: tuple: (bytes, bool)
        position 0 is the page content as bytes and position 1 if the function if an error occurs
        """
        out = None
        err = False
        if not self.__uri:
            log.append_warning(f"Object doesn't have a uri to ping")
            return out, True
        ident = self.__name if self.__name is not None else self.__uri
        try:
            params = ''
            if self.__params:
                params = parse.urlencode(self.__params)
            req = Request(f"{self.__uri}?{params}")
            if self.__headers:
                req.headers = self.__headers
            with request.urlopen(req) as response:
                if response.status != 200:
                    log.append_warning(
                        f"URL provided responded with Status Code {response.status}.\n\t\tURL: {ident}")
                    err = True
                out = response.read()
        except error.URLError as e:
            log.append_error(
                f"Failed to handle url\n\t\tURL: {ident}\n\t\tMSG: {str(e)}")
            err = True
        else:
            log.append_info(f"Ping: PASS - '{ident}'")

        return out, err

    @property
    def parameters(self):
        return self.__params

    @property
    def headers(self):
        return self.__headers

    @property
    def name(self):
        return self.__name


class _InvalidEndpoint(TryPing):
    """
    Empty helper class that is used rather than None for 'EndPoint' to prevent interruptions in loops
    This class inherits the 'TryPing' class which passes by default.
    """

    def __init__(self):
        pass
