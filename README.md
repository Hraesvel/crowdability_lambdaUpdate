# Crowdability lambdaUpdate

project code of the lambda function python version upgrade.

- updated Lambda functions are located in `./lambdaFunctions` directory
- `endpoint.py`, `log_details.py` are the helper classes for pinging and logging, these 2 scripts are part of the layer `endpointPingTools`
- automation of updating the layer and lambda function configuration is located in the `./.automate` directory

## Layer: `endpointPingTools`

This project uses a layer (`endpointPingTools`) to simplify updating the primary functions of this project. within this layer is 2 scripts

- `endpoint.py`
    - This class simplifies the configuration of an endpoint
    - provides extensive logging on the creation and performing a ping
- `log_details.py`
    - Custom logger

## Automation

location [`./automate`](./automate)

### overview
This contains 2 scripts that help to update layer `endpointPingTools`

- `check_endpoint.py`
    - compare the local file with that of the latest version on AWS and will upload a new version if necessary
- `lambda_function.py`
    - Script will look at each lambda function in file `lambdaFunctions.json` to see if the layer configuration is up to
      date
    - if a function is not using the latest layer version this will update the function configuration.


### Usage
Detailed setup guide located in `[`./automate`](./automate) directory 

These scripts use `AWS CLI v2` to handle credentials and `boto3` for AWS API.

dependencies:
- zip
- unzip
- AWS CLI V2
- autopep8
