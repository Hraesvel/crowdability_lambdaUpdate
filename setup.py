# from distutils.core import setup

from setuptools import find_packages, setup

setup(
    python_requires=">=3",
    name='Lambda Manager',
    version='0.1.1',
    author='Martin Smith',
    author_email='mcodesmith@gmail.com',
    packages=find_packages(),
    install_requires=[
        'click', 'boto3', 'importlib-resources', 'colorama'
    ],
    entry_points='''
[console_scripts]
lambdaManager=automate.console:cli
''',
    include_package_data=True,
)
