#!/usr/bin/env bash

profile="Crowdability"
if [ $# -gt 0 ]
then
  profile=$1
fi


if ! command -v lambdaManager &> /dev/null
then
    pip install --user --editable .
fi

lambdaManager -p "$profile" endpoint process
lambdaManager -p "$profile" lambda batch --push -a -c
lambdaManager -p "$profile" lambda config -ds -t 0 -f ./automate/resource/revert_lambdaFunctions.json
lambdaManager -p "$profile" cleanup