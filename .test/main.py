#!/usr/bin/env python3

from __future__ import print_function

import os
import sys

from endpoint import EndPoint as ep
from log_details import log

# KEY = os.environ['key']
# Placeholder
KEY = os.environ['key'] = "1234"


def lambda_handler(event, context):
    endpoints = [
        ep('https://youtube.com/results', "Cats", search_query='cats', ),
        ep('https://youtube.com/results', name="Fish", search_query='fish'),
        ep('https://youtube.com/results', search_query='dog', name='Dogs'),
        ep('https://youtube.com/results', search_query='bear'),
        ep("", "empty", search_query='bear'),
    ]
    for e in endpoints:
        print(e.try_ping())

    print(log)


if __name__ == '__main__':
    pass
    endpoints = [
        ep('https://youtube.com/results', "Cats", search_query='cats', ),
        ep('https://youtube.com/results', name="Fish", search_query='fish'),
        ep('https://youtube.com/results', search_query='dog', name='Dogs'),
        ep('https://youtube.com/results', search_query='bear', name='bear'),
        ep('https://hjkfaew.com', name='random'),
        ep('https://msmith.online/fdsf/', name='404', headers=dict(fish='cat', cat='dog')),
    ]
    for e in endpoints:
        u = e.try_ping()
    print(log)
    #
    # example
    # ping_endpoints("https://youtube.com")
    # ping_endpoints("https://youtube.com/results", search_query='cats')
    #
    # example of a fail
    # ping_endpoints("https://fdafdsafasdfklj.com")
    # ping_endpoints(None)
