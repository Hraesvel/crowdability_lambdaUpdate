"""
From webStartScrapers
"""
from os import environ

from endpoint import EndPoint as ep
from log_details import log

KEY = environ['key']
URLS = [ep(environ['url'], name='ingest_data/start', key=KEY)]


def lambda_handler(event, context):
    log.append_info(f'Performing ping on {len(URLS)}.')
    for u in URLS:
        u.try_ping()
    log.append_info('Process Complete')
    print(log)
