from os import environ
from endpoint import EndPoint as ep
from log_details import log

KEY = environ['key']
URLS = [
    ep('https://www.crowdability.com/admin_telesales/recurly_maintenance/update_subscriptions', key=KEY),
    ep('https://www.crowdability.com/admin_telesales/recurly_maintenance/update_billing_info', key=KEY)]


def lambda_handler(event, context):
    log.append_info(f'Performing ping on {len(URLS)}.')
    for u in URLS:
        u.try_ping()
    log.append_info('Process Complete')
    print(log)


if __name__ == "__main__":
    lambda_handler(None, None)
