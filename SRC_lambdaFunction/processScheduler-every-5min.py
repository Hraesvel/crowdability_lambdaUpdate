from os import environ

from endpoint import EndPoint as ep
from log_details import log

EXPECTED = environ['expected']
URLS = [ep(environ['url'], name='queueCheckCarts')]


def lambda_handler(event, context):
    log.append_info(f'Performing ping on {len(URLS)}.')
    for u in URLS:
        resp, err = u.try_ping()
        if str.encode(EXPECTED) not in resp:
            log.append_warning('Check failed!')
        else:
            log.append_info('Check pass!')
    log.append_info('Process Complete')
    print(log)
