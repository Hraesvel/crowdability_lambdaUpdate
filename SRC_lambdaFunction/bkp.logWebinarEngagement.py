from  __future__ import print_function

import urllib
import urllib2
import json
import os
import base64
import boto3
import datetime
import time
import hashlib

from botocore.client import Config

BUCKET_NAME = os.environ['bucket_name']
FOLDER = os.environ['folder_name']


def logEvent(event):
    event['created_at'] = datetime.datetime.utcnow().isoformat()
    event['position'] = int(event['position'])
    event['duration'] = int(event['duration'])

    json_data = json.dumps(event)

    e = event['identifier'] if (event['email'] == '') else event['email']

    t = str(time.time())
    m = hashlib.md5(t + event['content'] + e)
    f = m.hexdigest()
    filename = f + '.js'

    s3 = boto3.client('s3')
    s3.put_object(
        ACL='private',
        Body=json_data,
        Bucket=BUCKET_NAME,
        Key=FOLDER + '/' + event['content'] + '/' + filename
    )


def lambda_handler(event, context):
    logEvent(event['query'])

    return event['query']['callback'] + "({\"success:\":\"1\"})"
