from os import environ

from endpoint import EndPoint as ep
from log_details import log

KEY = environ['key1']
KEY2 = environ['key2']
WEBKEY = environ['webkey']

URLS = [
    # from hourlyPing
    ep(environ['url2'], name='adjust_editorial_space_ad_backend'),
    # from hourlyPing
    ep(environ['url3'], key=KEY, name='process_webinar_signups'),
    # from hourlyPing
    ep(environ['url4'], key=KEY2, name='add_active_contacts_to_newsletter'),
    # from queueUpdatePositionPrices
    ep(environ['url5'], key=KEY2, name='update_mca_position_prices'),
    # from IngestMaropostCampaignsData
    ep(environ['url7'], key=KEY2, name='get_sent_campaigns'),
    # from IngestMailData
    ep(environ['url8'], key=KEY2,
       name='ingest_mailing_data'),
    # from queueUpdatePositionPrices
    ep(environ['url6'], key=WEBKEY, name='portfolio_daily_high_updated'),
]


def lambda_handler(event, context):
    log.append_info(f'Performing ping on {len(URLS)}.')
    for u in URLS:
        u.try_ping()
    log.append_info('Process Complete')
    print(log)
