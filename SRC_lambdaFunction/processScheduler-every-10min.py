from os import environ

from endpoint import EndPoint as ep
from log_details import log

URLS = [ep(environ['site'], name='check_order_errors')]


def lambda_handler(event, context):
    log.append_info(f'Performing ping on {len(URLS)}  endpont.')
    for u in URLS:
        u.try_ping()
    log.append_info('Process Complete')
    print(log)
