from base64 import b64encode
from os import environ
from urllib import request

from endpoint import EndPoint as ep
from log_details import log

# TEMP for local testing
environ['api_key'] = '7af154b53bdc4c839bd1f8654c124295'
environ['mailgun_api_key'] = 'key-94f46b9484c8c2eff31d0e5ee02d417f'
environ['hipchat_token'] = 'sKCYepYfwnenL5CDvIKc0x6bMTcxQFEdcyq14PaX'

environ['url'] = "http://google.com"
environ['hipchat_url'] = "http://google.com"
environ['mailgun_url'] = "http://google.com"

# End of TEMP

API_KEY = b64encode(f"{environ['api_key']}:".encode("utf-8")).decode()
HIPCHAT_TOKEN = environ['hipchat_token']
MAILGUN_API_KEY = b64encode(
    f"api:{environ['mailgun_api_key']}".encode('utf-8')).decode()

URL = ep(environ['url'],
         name='accounts',
         headers=dict(
             Authorization=f'Basic {API_KEY}',
             Accept='application/xml'
))

HIPCHAT = ep(environ['hipchat_url'],
             name='hipchat',
             headers={'Content-Type': 'application/x-www-form-urlencoded'},
             message='(rageguy) Alert: Recurly API is unreachable.',
             message_format='text',
             color='red',
             auth_token=HIPCHAT_TOKEN
             )
MAILGUN = ep(environ['mailgun_url'],
             name='mailgun',
             headers={
                 'Authorization': f'Basic {MAILGUN_API_KEY}',
                 'Content-Type': 'application/x-www-form-urlencoded'
},
    # 'from' is a keyword in python
    **{'from': 'checker@crowdability.com', 'to': 'blizzardzz@gmail.com'},
    subject='Alert: Recurly API is unreachable.',
    text='Alert: Recurly API is unreachable.',
)


def lambda_handler(event, context):
    log.append_info("Performing every 1 min Scheduled task")
    result, err = URL.try_ping()

    if not err:
        log.append_info('Process Complete')
        return

    log.append_error('Failed to reach accounts endpoint.')

    url_s3 = 'http://crowdability-checkers.s3.amazonaws.com/recurly.js'

    with request.urlopen(url_s3) as lines:
        for line in lines:
            log.append_info(line)

            if line == '1':
                # Hip Chat notification
                log.append_info("Performing Hip Chat notification")
                HIPCHAT.try_ping()

                log.append_info("Performing Mailgun notification")
                # Mailgun notification
                MAILGUN.try_ping()


if __name__ == "__main__":
    lambda_handler(None, None)
